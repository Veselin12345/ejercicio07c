// Inicializamos módulos a utilizar
var publicoRouter = require('express').Router();
var UsuarioController = require('../controladores/usuario.js');
// Creamos rutas a utilizar y su controlador
publicoRouter.route('/')
.get(UsuarioController.getIndex) //GET
publicoRouter.route('/usuarios')
.get(UsuarioController.getUsuarios) //GET
publicoRouter.route('/usuarioseditar')
.get(UsuarioController.getUsuariosEditar) //GET
.post(UsuarioController.postUsuariosEditar) //POST 
publicoRouter.route('/usuarioseditar/:loki')
.get(UsuarioController.getUsuariosEditar); //GET
module.exports = publicoRouter;