// Inicializamos módulos a utilizar
var restler = require('restler');
// Creamos funciones del controlador
exports.getIndex = function(req, res){ //GET
	res.render('index');
};
exports.getUsuarios = function(req, res){ //GET
	restler.get('http://' + req.app.get('IP') + ':' + req.app.get('PORT') + '/api/usuarios/').on('complete', function(data){
		res.render('usuarios', {
			data: data
		});
	});
};
exports.getUsuariosEditar = function(req, res){ //GET
	if(req.params.loki){
		restler.get('http://' + req.app.get('IP') + ':' + req.app.get('PORT') + '/api/usuarios/' + req.params.loki).on('complete', function(data){
			res.render('usuarioseditar', {
				data: data
			});
		});
	}
	else
		res.render('usuarioseditar');
};
exports.postUsuariosEditar = function(req, res){ //POST
	if(req.body._method == 'post'){ //POST
		restler.post('http://' + req.app.get('IP') + ':' + req.app.get('PORT') + '/api/usuarios/', {
			data: {
				nombre: req.body.nombre,
				edad: req.body.edad
			},
		}).on('complete', function(data){
			res.redirect("/usuarios");
		});
	}
	if(req.body._method == 'put'){ //PUT simulado
		restler.put('http://' + req.app.get('IP') + ':' + req.app.get('PORT') + '/api/usuarios/' + req.body.loki, {
			data: {
				nombre: req.body.nombre,
				edad: req.body.edad
			},
		}).on('complete', function(data){
			res.redirect("/usuarios");
		});
	}
	if(req.body._method == 'delete'){ //DELETE simulado
		restler.del('http://' + req.app.get('IP') + ':' + req.app.get('PORT') + '/api/usuarios/' + req.body.loki).on('complete', function(data){
			res.redirect("/usuarios");
		});
	}
};